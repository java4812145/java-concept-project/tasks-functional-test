package br.robertoqa.tasks.functional;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.Duration;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

public class TasksTest {

	public WebDriver acessarAplicacao() throws MalformedURLException, URISyntaxException {
		URI url = new URI("http://192.168.2.102:4444");
		ChromeOptions options = new ChromeOptions();
		WebDriver driver = new RemoteWebDriver(url.toURL(), options);
		driver.navigate().to("http://192.168.2.102:8001/tasks");
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		return driver;
	}

	@Test
	public void deveSalvarTarefaComSucesso() throws MalformedURLException, URISyntaxException {
		WebDriver driver = acessarAplicacao();
		driver.findElement(By.id("addTodo")).click();
		driver.findElement(By.id("task")).sendKeys("Nova tarefa");
		driver.findElement(By.id("dueDate")).sendKeys("10/10/2030");
		driver.findElement(By.id("saveButton")).click();
		String mensagem = driver.findElement(By.id("message")).getText();
		Assert.assertEquals("Success!", mensagem);
		driver.quit();
	}

	@Test
	public void naoDeveSalvarTarefaSemDescricao() throws MalformedURLException, URISyntaxException {
		WebDriver driver = acessarAplicacao();
		driver.findElement(By.id("addTodo")).click();
		driver.findElement(By.id("dueDate")).sendKeys("10/10/2030");
		driver.findElement(By.id("saveButton")).click();
		String mensagem = driver.findElement(By.id("message")).getText();
		Assert.assertEquals("Fill the task description", mensagem);
		driver.quit();
	}

	@Test
	public void deveSalvarTarefaSemData() throws MalformedURLException, URISyntaxException {
		WebDriver driver = acessarAplicacao();
		driver.findElement(By.id("addTodo")).click();
		driver.findElement(By.id("task")).sendKeys("Nova tarefa");
		driver.findElement(By.id("saveButton")).click();
		String mensagem = driver.findElement(By.id("message")).getText();
		Assert.assertEquals("Fill the due date", mensagem);
		driver.quit();
	}

	@Test
	public void deveSalvarTarefaComDataPassada() throws MalformedURLException, URISyntaxException {
		WebDriver driver = acessarAplicacao();
		driver.findElement(By.id("addTodo")).click();
		driver.findElement(By.id("task")).sendKeys("Nova tarefa");
		driver.findElement(By.id("dueDate")).sendKeys("23/01/1991");
		driver.findElement(By.id("saveButton")).click();
		String mensagem = driver.findElement(By.id("message")).getText();
		Assert.assertEquals("Due date must not be in past", mensagem);
		driver.quit();
	}

}
